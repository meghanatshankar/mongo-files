var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var express = require('express');
var app = express();
var bodyParser = require('body-parser')
app.use(bodyParser.json())

app.listen(8000, (err) => {
    console.log('server is listening on port', 4000);
});
var dbo;

MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    dbo = db.db("myDb");
    dbo.createCollection("persons", function (err, res) {
        console.log("Collection created!");
    });
});

app.post('/addAll', function (req, res) {
    var personData = require('./nodeProject/persons.json');
    dbo.collection("persons").insertMany(personData, function (err, r) {
        if (err) throw err;
        if (res) {
            console.log("Documents inserted");
            res.send('The file has been saved!');
        }

    });
})

app.get('/user',function(req,res){
    dbo.collection("persons").find({}).toArray(function(err,result){
        if (err) throw err;
        res.send(result);
    })
    
})


app.post('/user', function (req, res) {
    personData=(req.body);
    dbo.collection("persons").insertOne(personData, function (err, r) {
        if (err) throw err;
        if (res) {
            console.log("Document inserted");
            res.send('The file has been saved!');
        }
    });    
})
app.delete('/user/:email', function (req, res){
    personData={email:req.params.email}
    dbo.collection("persons").deleteOne(personData, function(err, obj){
        if (err) throw err;
        console.log("Document deleted");
        res.send('The file has been saved!');
    })    
})
    
app.put('/user/:email', function (req, res){
    personData={email:req.params.email}
    newValues={$set:(req.body)}
    console.log(req.body);
    console.log(personData);
    dbo.collection("persons").updateOne(personData, newValues, function(err, obj){
        if (err) throw err;
        console.log("Document updated");
        res.send('The file has been saved!');    
    })    
})  